# pertama kali untuk file upload image kita buat utis.py dulu
from django.db import models
from django.utils.deconstruct import deconstructible
from uuid import uuid4
import os


@deconstructible
class PathForFileModel(object):

	def __init__(self, sub_path):
		self.path = sub_path

	def __call__(self, instance, filename):
		ext = filename.split('.')[-1]
		# uuid : set filename as random string
		filename = '{}.{}'.format(uuid4().hex, ext)
		# return the whole path to the file
		return os.path.join(self.path, filename)

# jika hapus data yang ada gambarnya, maka gambar di folder juga ikut hilang , ini masih belum bisa
# perlu dihapus
class ImageField(models.ImageField):
	def save_form_data(self, instance, data):
		if data is not None: 
			file = getattr(instance, self.attname)
			if file != data:
				file.delete(save=False)
		super(ImageField, self).save_form_data(instance, data)
