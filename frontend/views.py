from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from .models import JenisBarang, KategoriBarang, Barang, Jabatan, DataPegawai, Kontak

from .forms import KontakForm

# Create your views here.
def beranda(request):
	pulsa_reguler_list = Barang.objects.filter(nama_kategori_barang__nama_kategori_barang="PULSA REGULER")
	paket_data_list = Barang.objects.filter(nama_kategori_barang__nama_kategori_barang="PAKET DATA")
	return render(request, 'frontend/index.html', {'pulsa': pulsa_reguler_list, 'paket': paket_data_list})

def tentang(request):
	pegawai_list = DataPegawai.objects.all()
	return render(request, 'frontend/about.html', {'pegawai': pegawai_list})

def kontak(request):
	kontak_list = Kontak.objects.all()
	if request.method == "POST":
		form = KontakForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.author = request.user
			post.published_date = timezone.now()
			post.save()
			return redirect('kontak')
	else:
		form = KontakForm()
	return render(request, 'frontend/contact.html', {'kontak': kontak_list, 'form': form})

# def kontak_new(request):
# 	if request.method == "POST":
# 		form = KontakForm(request.POST)
# 		if form.is_valid():
# 			post = form.save(commit=False)
# 			post.author = request.user
# 			post.published_date = timezone.now()
# 			post.save()
# 			return redirect('kontak', pk=post.pk)
# 	else:
# 		form = KontakForm()
# 	return render(request, 'frontend/contact.html', {'form': form})

# def kontak_edit(request, pk):
# 	post = get_object_or_404(Post, pk=pk)
# 	if request.method == "POST":
# 		form = KontakForm(request.POST, instance=post)
# 		if form.is_valid():
# 			post = form.save(commit=False)
# 			post.author = request.user
# 			post.published_date = timezone.now()
# 			post.save()
# 			return redirect('kontak', pk=post.pk)
# 	else:
# 		form = KontakForm(instance=post)
# 	return render(request, 'frontend/contact.html', {'form': form})