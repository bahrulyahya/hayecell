from django.conf import settings
from django.db import models
from django.utils import timezone


# dibawah ini utils PathForFileModel untuk memindah gambar ke folder yag sudah ditentukan dan ImageField
from frontend.utils import PathForFileModel
# Create your models here.

class JenisBarang(models.Model):
	nama_jenis_barang = models.CharField(max_length=200, verbose_name="Nama Jenis Barang", null=True, blank=True)
	keterangan = models.TextField()

	def __str__(self):
		return self.nama_jenis_barang

	class Meta:
		ordering = ['id']
		verbose_name = 'Jenis Barang'
		verbose_name_plural = 'Jenis Barang'


class KategoriBarang(models.Model):
	nama_jenis_barang = models.ForeignKey(JenisBarang, on_delete=models.CASCADE, verbose_name="Jenis Barang")
	nama_kategori_barang = models.CharField(max_length=200, verbose_name="Nama Kategori Barang", null=True, blank=True)
	keterangan = models.TextField()

	def __str__(self):
		return self.nama_kategori_barang

	class Meta:
		ordering = ['id']
		verbose_name = 'Kategori Barang'
		verbose_name_plural = 'Kategori Barang'



class Barang(models.Model):
	nama_kategori_barang = models.ForeignKey(KategoriBarang, on_delete=models.CASCADE, verbose_name="Kategori Barang")
	kode_barang = models.CharField(max_length=200, verbose_name="Kode Barang", null=True, blank=True)
	gambar	= models.ImageField(upload_to=PathForFileModel('image'), max_length=255, null=True, blank=True)
	nama_barang = models.CharField(max_length=200, null=True, blank=True)
	deskripsi = models.TextField()
	harga_sebelum_diskon = models.IntegerField(verbose_name="Sebelum diskon")
	harga_sesudah_diskon = models.IntegerField(verbose_name="Sesudah diskon")
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True, null=True)

	def publish(self):
		self.published_date = timezone.now()
		self.save()

	# jika gambar tidak ada, maka di default gambar no-image.jpg
	def get_foto(self):
		if self.gambar:
			return settings.MEDIA_URL+str(self.gambar)
		return settings.STATIC_URL+"images/no-image.png"

	def __str__(self):
		return self.nama_barang

	class Meta:
		ordering = ['id']
		verbose_name = 'Barang'
		verbose_name_plural = 'Barang'


class Jabatan(models.Model):
	nama_jabatan = models.CharField(max_length=200, verbose_name="Nama Jabatan")
	keterangan = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.nama_jabatan

	class Meta:
		ordering = ['id']
		verbose_name = 'Jabatan'
		verbose_name_plural = 'Jabatan'

class DataPegawai(models.Model):
	gambar	= models.ImageField(upload_to=PathForFileModel('image'), max_length=255, null=True, blank=True)
	nama_pegawai = models.CharField(max_length=200, verbose_name="Nama Pegawai", null=True, blank=True)
	nama_jabatan = models.ForeignKey(Jabatan, on_delete=models.CASCADE, verbose_name="Jabatan")
	deskripsi = models.TextField(verbose_name="Deskripsi", null=True, blank=True)
	url_fb = models.CharField(max_length=200,verbose_name="Url Facebook", null=True, blank=True)
	url_twitter = models.CharField(max_length=200,verbose_name="Url Twitter", null=True, blank=True)
	url_ig = models.CharField(max_length=200,verbose_name="Url Instagram", null=True, blank=True)
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True, null=True)

	def publish(self):
		self.published_date = timezone.now()
		self.save()

	# jika gambar tidak ada, maka di default gambar no-image.jpg
	def get_foto(self):
		if self.gambar:
			return settings.MEDIA_URL+str(self.gambar)
		return settings.STATIC_URL+"images/no-image.png"

	def __str__(self):
		return self.nama_pegawai

	class Meta:
		ordering = ['id']
		verbose_name = 'Data Pegawai'
		verbose_name_plural = 'Data Pegawai'


class Kontak(models.Model):
	nama_lengkap = models.CharField(max_length=200, verbose_name="Nama Lengkap Anda", null=True, blank=True)
	email = models.CharField(max_length=200, verbose_name="Email Valid Anda",unique=True, error_messages={'unique':"Email yang anda gunakan sudah terdaftar!."})
	pesan = models.TextField()

	def __str__(self):
		return self.nama_lengkap

	class Meta:
		ordering = ['id']
		verbose_name = 'Kontak'
		verbose_name_plural = 'Kontak'