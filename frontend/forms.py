from django import forms

from .models import Kontak

class KontakForm(forms.ModelForm):
	nama_lengkap = forms.CharField(max_length=5, widget=forms.TextInput(attrs={'id':'bahrulyahya', 'class': 'form-control'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'id':'bahrulyahya', 'class': 'form-control'}))
	pesan = forms.CharField(widget=forms.Textarea(attrs={'id':'bahrulyahya', 'class': 'form-control'}))
	class Meta:
		model = Kontak
		fields = ('nama_lengkap', 'email', 'pesan',)


        