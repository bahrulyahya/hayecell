from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Barang, JenisBarang, KategoriBarang, Jabatan, DataPegawai, Kontak
# Register your models here.
class JenisBarangAdmin(admin.ModelAdmin):
	# list_display_links = None
	list_display = ('nama_jenis_barang', 'keterangan')
	search_fields = ('nama_jenis_barang', 'keterangan')

class KategoriBarangAdmin(admin.ModelAdmin):
	# list_display_links = None
	list_display = ('nama_kategori_barang','nama_jenis_barang',  'keterangan')
	search_fields = ('nama_kategori_barang','nama_jenis_barang',  'keterangan')

class BarangAdmin(admin.ModelAdmin):
	# list_display_links = None
	search_fields = ('nama_barang', )
	list_filter = ('nama_kategori_barang',)
	list_display = ('kode_barang', 'nama_barang', 'nama_kategori_barang', 'get_foto', 'deskripsi', 'harga_sebelum_diskon', 'harga_sesudah_diskon', 'created_date', 'published_date')
	
	def get_foto(self, obj):
		str_html = ""
		if obj.gambar:
			url = obj.gambar.url
			str_html = """
				<img src="{}" class="image-list-item" />
			""".format(url)
		return mark_safe(str_html)
	get_foto.short_description='Foto'

class JabatanAdmin(admin.ModelAdmin):
	# list_display_links = None
	list_display = ('nama_jabatan', 'keterangan')
	search_fields = ('nama_jabatan', 'keterangan')

class DataPegawaiAdmin(admin.ModelAdmin):
	# list_display_links = None
	search_fields = ('nama_pegawai', )
	list_filter = ('nama_jabatan',)
	list_display = ( 'nama_pegawai', 'gambar', 'nama_jabatan', 'deskripsi', 'url_fb', 'url_twitter', 'url_ig')
	 
	def get_foto(self, obj):
		str_html = ""
		if obj.gambar:
			url = obj.gambar.url
			str_html = """
				<img src="{}" class="image-list-item" />
			""".format(url)
		return mark_safe(str_html)
	get_foto.short_description='Foto'


class KontakAdmin(admin.ModelAdmin):
	# list_display_links = None
	list_display = ('nama_lengkap', 'email', 'pesan',)
	search_fields = ('nama_lengkap', 'email', 'pesan',)

admin.site.register(Barang, BarangAdmin)
admin.site.register(JenisBarang, JenisBarangAdmin)
admin.site.register(KategoriBarang, KategoriBarangAdmin)
admin.site.register(Jabatan, JabatanAdmin)
admin.site.register(DataPegawai, DataPegawaiAdmin)
admin.site.register(Kontak, KontakAdmin)