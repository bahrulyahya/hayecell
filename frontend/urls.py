from django.urls import path
from . import views

urlpatterns = [
	path('', views.beranda, name='beranda'),
	path('tentang/', views.tentang, name='tentang'),
	path('kontak/', views.kontak, name='kontak'),

	# path('post/<int:pk>/', views.post_detail, name='post_detail'),

	# path('post/new/', views.post_new, name='post_new'),

	# path('post/<int:pk>/edit/', views.post_edit, name='post_edit'),

]